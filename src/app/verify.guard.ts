import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot,Router} from "@angular/router";
import { Observable } from "rxjs/Rx";
import {DbService} from "./db.service";
import { Injectable } from '@angular/core';

@Injectable()
export class VerifyGuard implements CanActivate {
  constructor(private dbService:DbService,private route:Router){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {                           
      if(route.params['id']==="" || this.dbService.getStudentById(route.params['id']).length===0)
      {
        this.route.navigate(['error']);
        return false;
      }
      else
      {
        console.log(route.params['id']);
         return true;
      }     
  }
}
