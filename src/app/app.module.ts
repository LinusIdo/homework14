import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from './app.component';
import { HomeComponent } from './home.component';
import { StudentsComponent } from './students.component';
import { DbService } from './db.service';
import { ProfileComponent } from './profile.component';
import { VerifyGuard } from "./verify.guard";
import { ErrorComponent } from './error.component';

const MY_ROUTES: Routes = [
     { path: '', component: HomeComponent },
     { path: 'students', component: StudentsComponent},
     { path: 'students/profile/:id', component: ProfileComponent,canActivate:[VerifyGuard]},
     { path: 'error',component:ErrorComponent},
     { path: '**', redirectTo: 'error' }];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StudentsComponent,
    ProfileComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(MY_ROUTES)
  ],
  providers: [DbService,VerifyGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
