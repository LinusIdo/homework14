import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DbService } from './db.service';
@Component({
  selector: 'app-profile',
  template: `
  <h2>Student Profile</h2>
  <p>
   ID:{{_student[0].id}}<br/> 
   Name:{{_student[0].name}}<br/>
   Email:{{_student[0].email}}<br/>
   </p>
  `,
  styles: []
})
export class ProfileComponent implements OnInit {
  private _id: string = null;
  private _student: Object=null;
  constructor(private route: ActivatedRoute, private dbService: DbService) {
    route.params.subscribe(params => {
      this._id = params['id'];
      this._student = this.dbService.getStudentById(this._id);     
    });
  }
  ngOnInit() {

  }

}
