import { Component, OnInit } from '@angular/core';
import { DbService } from './db.service';
import { Router } from "@angular/router";
import { Observable } from "rxjs/Rx";

@Component({
  selector: 'app-students',
  template: `
  <div>
    <ul>
    <li *ngFor="let std of _students"><a [routerLink]="['profile',std.id]">{{std.name}}</a></li>
    </ul>      
    </div>    
    <router-outlet></router-outlet>
  `,
  styles: ['ul {list-style-type:none}']
})
export class StudentsComponent implements OnInit {
  _students: Object[] = [];
  constructor(private dbService: DbService) {
    this._students = this.dbService.getData().slice(0);
  }
  ngOnInit() {
  }

}
