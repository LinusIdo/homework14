import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <div>
    <h2>The founder of Maharishi University of Management was Maharishi Mahesh Yogi.</h2>
    <p>His message was that the purpose of life is the expansion of happiness, and that every human being can effortlessly unfold his or her infinite potential. Learn more about Maharishi Mahesh Yogi.</p>
    </div>
    <div>
    <h3>Introduction</h3>
    <p>
      For more than 40 years, Maharishi University of Management (MUM) has pioneered a new approach to learning, called Consciousness-Based℠ education, which supplies a missing element to education.
      All students and faculty at MUM practice the Transcendental Meditation® (TM) technique. This simple technique offers remarkable benefits for education, as extensive research shows — integrated brain functioning, increased creativity and intelligence, reduced stress, improved learning ability, improved academic performance (GPA), improved ability to focus, improved health, and many more.
      Accredited by the Higher Learning Commission, MUM offers Bachelor’s, Master’s, and PhD degrees in a variety of traditional fields, including Business, Media and Communications, Art, Literature, Education, and Computer Science. MUM has developed some exciting new disciplines as well, including Sustainable Living and Maharishi Vedic Science.
    </p>
    </div>
  `,
  styles: []
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
