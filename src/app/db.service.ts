import { Injectable } from '@angular/core';

@Injectable()
export class DbService {
  private _studentData:Object[];
  constructor() { 
    this._studentData=[{id:'1',name:'Sunil Thapa',email:'sunilmagar048@gmail.com'},
    {id:'2',name:'Rajeev Tamrakar',email:'rajeev@gmail.com'},
    {id:'3',name:'Sanjay Rana',email:'sanjay@gmail.com'},
    {id:'4',name:'Devi Khadka',email:'devi@gmail.com'},
    {id:'',name:'Ranjan Sapkota',email:'ranjan@gmail.com'}
    ];
  }
  getData(){
    return this._studentData;
  }
  getStudentById(id:string){
    return this._studentData.filter(s=>s['id']===id.toString());  
  }
}
