import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error',
  template: `
    <h2>
      Oops! either ID is not passed or is wrong.
    </h2>
  `,
  styles: ['h2 {color:red}']
})
export class ErrorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
