import { HW14AppPage } from './app.po';

describe('hw14-app App', function() {
  let page: HW14AppPage;

  beforeEach(() => {
    page = new HW14AppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
